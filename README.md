# Application Letter Generator

A lazy tool to generate application letters when applying for work at different businesses.
It generates a pdf from a template.

# Usage

* A template file will be created on first run.
* Edit the template file after your own likes and needs. The template is in `LaTeX` format.
* Add placeholders in the template. The placeholders should be in this format, `_placeholdername_`. A word wrapped in underscores.
* Run the script again.
* The application will find the placeholders and ask you to enter information according to those placeholders.
* A `pdf` will be generated according to your template.

#  Dependencies

Right now, this script needs `pdflatex` to generate the `pdf`.

# TODO

* Add function to choose placeholder delimiter. Right now it's underscore `_`
* Clean up the code
