#!/usr/bin/env python

import subprocess as sp
from os.path import exists
import glob
import re

set_of_vars = set()
dict_of_values = {}
file_in = 'template.tex'

def convert_var(var):
    return var[1:-1].lower().replace('_', ' ')

def populate_dict():
    for var in set_of_vars:
        var = convert_var(var)
        value = input(f'Enter {var}: \n')
        dict_of_values[var]=value

def generate_template_file():
    if not exists(file_in):
        print(f'{file_in} not found, creating...')
        with open(file_in, 'w'): pass
        print(f'Edit the {file_in} file after your likes and needs, then run the script again.')
        exit()

def template():
    while True:
        ready = input(('Edit the "template.tex" file before running this script.\n'
                    'If the file is ready, type (y)es, else type (n)o.\n')).lower()

        match ready:
            case 'yes'|'y' : break
            case 'no'|'n' : 
                print(f'Edit the {file_in} file and then run the script again.')
                exit()
            case _ :
                print('You most enter (y)es or (n)o.\n')

def output_file():
    global file_out
    file_out = input('Enter a output filename: \n').lower()
    if file_out[-4:] != '.tex':
        file_out = file_out + '.tex'

def populate_set():
    with open(file_in, 'r') as f:
        global lines
        lines = f.readlines()
        for line in lines:
            found_vars = re.findall(r'\b_(?:\w*_)?\b', line)
            for var in found_vars:
                set_of_vars.add(var)

def replace_vars():
    with open(file_out, 'w') as f:
        for line in lines:
            re_line = re.sub("[^\w\s]", " ", line)
            found_words = (var for var in re_line.split() if var in set_of_vars)
            for word in found_words:
                line = line.replace(word, dict_of_values[convert_var(word)])
            f.write(line)

def cleanup():
    files_cleanup = glob.glob(f'{file_out[:-3]}*[!pdf]')
    for file in files_cleanup:
        sp.run(['rm', file])
    print('Cleanup done, exiting...')

def generate_and_cleanup():
    print('Generating pdf document...')
    try:
        sp.run(['pdflatex', '-interaction=nonstopmode', file_out], stdout=sp.DEVNULL, timeout=10)
        print('Document created, cleaning up...')
        cleanup()
    except sp.TimeoutExpired:
        print(f'Timeout expired, could not generate pdf. Check the {file_in} for errors.')
        cleanup()
        exit()
    except:
        print(f'Something went wrong, could not generate pdf. Check the {file_in} for errors.')
        cleanup()
        exit()

def Main():
    generate_template_file()
    template()
    output_file()
    populate_set()
    populate_dict()
    replace_vars()
    generate_and_cleanup()

if __name__ == '__main__':
    Main()
